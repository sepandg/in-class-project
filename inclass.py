age_order = ['[0-10)','[10-20)', '[20-30)','[30-40)','[40-50)','[50-60)','[70-80)','[80-90)','[80-90)','[90-100)']
age_order_cat = {a:b for a, b in zip(age_order, range(1, 1+len(age_order)))}
class ordinal_age(BaseEstimator, TransformerMixin):

    def __init__(self):
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        _X = X.copy()


        for i in X.columns:

            _X[i] = _X[i].map(age_order_cat)

        return _X


weight_order = ['[0-25)','[25-50)', '[50-75)','[75-100)','[100-125)','[125-150)','[150-175)','?']
weight_order_cat = {a:b for a, b in zip(weight_order, range(1, 1+len(weight_order)))}
class ordinal_weight(BaseEstimator, TransformerMixin):

    def __init__(self):
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        _X = X.copy()


        for i in X.columns:

            _X[i] = _X[i].map(weight_order_cat)

        return _X

age_imputer = SimpleImputer(strategy='constant',fill_value=0)
weight_imputer = SimpleImputer(strategy='constant',fill_value=8)
